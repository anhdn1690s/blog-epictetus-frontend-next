import { fetchAPI } from '@/utils/fetchQuery';
import React from 'react'
import Search from '../components/search/search'

export async function getServerSideProps({ query: { q } }) {
    // const postsRes = await fetchAPI("/posts?_q=" + q);
    const postsRes = await fetchAPI("/posts", {
        populate: "*",
        _q: q,
    });
    return {
        props: {
            postsRes
        }
    }
}
const PageSearch = ({ postsRes }) => {
    return (
        <Search data={postsRes}></Search>
    )
}

export default PageSearch