import { fetchAPI } from "@/utils/fetchQuery";
import Category from '../../components/category/category';



const PageCategory = ({ category, }) => {

    return (
        <Category data={category} />
    );
};

export async function getStaticPaths() {
    const categoriesRes = await fetchAPI("/categories", { fields: ["slug"] });

    return {
        paths: categoriesRes.data.map((category) => ({
            params: {
                slug: category.attributes.slug,
            },
        })),
        fallback: false,
    };
}

export async function getStaticProps({ params }) {
    const matchingCategories = await fetchAPI("/categories", {
        filters: { slug: params.slug },
        populate: {
            posts: {
                populate: "*",
            },
        },
    });
    const allCategories = await fetchAPI("/categories");

    return {
        props: {
            category: matchingCategories.data[0],
            categories: allCategories,
        },
        revalidate: 1,
    };
}

export default PageCategory;