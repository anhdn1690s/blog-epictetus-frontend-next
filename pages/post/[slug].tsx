
import { useRouter } from 'next/router';
import React from 'react';
import Error from 'next/error'
import PostDetail from '../../components/postDetail/postDetail';
import { fetchAPI } from '@/utils/fetchQuery';

const PagePostDetail = ({ article, categories }) => {
    return (
        <PostDetail data={article} />
    )
}


export async function getStaticPaths() {
    const articlesRes = await fetchAPI("/posts", { fields: ["slug"] });

    return {
        paths: articlesRes.data.map((article) => ({
            params: {
                slug: article.attributes.slug,
            },
        })),
        fallback: false,
    };
}

export async function getStaticProps({ params }) {
    const articlesRes = await fetchAPI("/posts", {
        filters: {
            slug: params.slug,
        },
        populate: ["img", "category"],
    });
    const categoriesRes = await fetchAPI("/categories");

    return {
        props: { article: articlesRes.data[0], categories: categoriesRes },
        revalidate: 1,
    };
}

export default PagePostDetail