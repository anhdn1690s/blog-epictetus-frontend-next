import Head from 'next/head'
import Image from 'next/image'
import axios from 'axios';
import styles from '@/styles/Home.module.css'
import HomePage from '../components/home/home'
import { fetchAPI, fetcher } from '@/utils/fetchQuery';
import Seo from '@/components/seo/seo';


export default function Home({ posts }) {
  return (
    <>
      <main className={styles.main}>
        <HomePage data={posts} />
      </main>
    </>
  )
}

export async function getStaticProps() {
  // Run API calls in parallel
  const [articlesRes] = await Promise.all([
    fetchAPI("/posts", { populate: "*" }),
  ]);

  return {
    props: {
      posts: articlesRes,
    },
    revalidate: 1,
  };
}
