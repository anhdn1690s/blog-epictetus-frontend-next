import { getStrapiURL } from "../utils/fetchQuery";

export function getStrapiMedia(media) {
  if (!media || !media.data) {
    return '';
  }
  const { url } = media.data.attributes;
  const imageUrl = url.startsWith("/") ? getStrapiURL(url) : `/${url}`;
  return imageUrl;
}