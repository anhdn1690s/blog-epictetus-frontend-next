import classes from './pagination.module.scss'

const Pagination = ({ items, pageSize, currentPage, onPageChange }) => {
    const pagesCount = Math.ceil(items / pageSize); // 100/10

    if (pagesCount === 1) return null;
    const pages = Array.from({ length: pagesCount }, (_, i) => i + 1);
    return (
        <div>
            <ul className={classes.pagination}>
                {pages.map((page) => (
                    <li
                        key={page}
                        className={
                            page === currentPage ? classes.pageItemActive : classes.pageItem
                        }
                        onClick={() => onPageChange(page)}
                    >
                        <a className={classes.pageLink} >
                            {page}
                        </a>
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default Pagination;