import React, { FunctionComponent } from 'react'
import Footer from '../footer/footer'
import { Header } from '../header/header'
import classes from './layout.module.scss'
import bgImg from '../../assets/bg_theme.png'

export const LayoutComponent = ({ children, data }) => {
    return (
        <div className={classes.nextRoot}>
            <div className={classes.mainRoot}>
                <Header dataCate={data} />
                {children}
                <Footer />
            </div>
        </div>
    )
}