import React from 'react'
import classes from './category.module.scss'
import PostCard from '../postCard/postCard'

const Category = ({ data }) => {
    return (
        <div className={classes.rootCategory}>
            <h2>Ui Design</h2>
            <div className={classes.rootPostCard}>
                {data.attributes.posts.data.map((post, index) => (
                    <PostCard post={post.attributes} key={index} />
                ))}
            </div>
        </div>
    )
}

export default Category