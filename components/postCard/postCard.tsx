import React from 'react'
import classes from './postCard.module.scss'
import moment from 'moment'
import NextImage from '../image/image'
import Link from 'next/link';
import Skeleton from 'react-loading-skeleton'


type Post = {
  [x: string]: any
  id: number,
  title: string,
  healine: string,
  slug: string,
}

const PostCard = ({ post }: { post: Post }) => {
  return (

    <div className={classes.rootPostCard}>
      {
        <Link href={`/post/${post.slug}`}>
          <NextImage image={post.img} />
        </Link>
        ??
        <Skeleton width="100%" height="180px" baseColor="#374151" highlightColor="#303844;"></Skeleton>
      }

      <div className={classes.info}>
        <p className={classes.cateAndDate}>{post.category.data.attributes.name ?? <Skeleton width="100%" height="180px" baseColor="#374151" highlightColor="#303844;"></Skeleton>}</p>
        <svg width="4" height="5" viewBox="0 0 4 5" fill="none" xmlns="http://www.w3.org/2000/svg">
          <rect y="0.5" width="4" height="4" rx="2" fill="white" fillOpacity="0.6" />
        </svg>
        <p className={classes.cateAndDate}>{moment(post.createdAt).format('MMM DD, YYYY')}</p>
      </div>
      <h3 className={classes.title}>

        <Link href={`/post/${post.slug}`}>
          {post.title ?? <Skeleton width="100%" height="15px" baseColor="#374151" highlightColor="#303844;"></Skeleton>}
        </Link>
      </h3>

      <p className={classes.sub}>
        {post.healine ?? <Skeleton width="100%" height="15px" baseColor="#374151" highlightColor="#303844;"></Skeleton>}
      </p>
    </div>
  )
}

export default PostCard