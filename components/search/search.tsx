import React from 'react'
import PostCard from '../postCard/postCard'
import classes from './search.module.scss'

const Search = ({ data }) => {

    console.log("postsRes", data)
    return (
        <div className={classes.rootSearch}>
            {data.data.map((post, index) => (
                <PostCard post={post.attributes} key={index} />
            ))}
        </div>
    )
}

export default Search