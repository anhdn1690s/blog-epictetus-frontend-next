import React, { useEffect, useState } from 'react'
import Link from 'next/link'
import classes from './header.module.scss'
import { MenuMobile } from './menuMobile'
import menuIcon from '../../assets/menu.svg'
import logo from '../../assets/logo-one.png'
import search from '../../assets/search.svg'
import { useHeaderState } from '@/hooks/stores/useHeaderState'

export const Header = ({ dataCate }) => {
  const setMobileLeftDrawer = useHeaderState(
    (state) => state.setMobileLeftDrawer
  );

  const category = [{ name: "UI Design", slug: 'UI-Design' }, { name: "Front-end", slug: 'Front-end' }, { name: "Back-end", slug: 'Back-end' }, { name: "Lainnya", slug: 'Lainnya' }]

  return (
    <div className={classes.rootHeader}>

      <div className={classes.iconMenu} onClick={() => {
        setMobileLeftDrawer(true);
      }}>
        <img src={menuIcon.src} alt="Img" />
      </div>
      <div className={classes.logo}>
        <Link href={"/"}>
          <img src={logo.src} alt="logo" />
        </Link>
        <div className={classes.nav}>
          <ul>
            {dataCate.global.map((category) => (
              <li key={category.slug}>
                <Link href={`/category/${category.attributes.slug}`}>
                  {category.attributes.name}
                </Link>
              </li>
            ))}
          </ul>
        </div>
      </div>
      <div className={classes.iconSearch} >
        <img src={search.src} alt="img" />
      </div>
      <MenuMobile key="mobile-menu" menuData={dataCate.global} />
    </div>
  )
}
