import React from 'react'
import moment from 'moment'
import classes from './postDetail.module.scss'
import ReactMarkdown from 'react-markdown'
import { getStrapiMedia } from '@/utils/media'
import NextImage from "next/image";

const PostDetail = ({ data }) => {
    const imageUrl = getStrapiMedia(data.attributes.img);
    console.log("data", data)

    return (
        <div className={classes.rootPostDetail}>
            <div className={classes.info}>
                <p>{data.attributes.category.data.attributes.name}</p>
                <svg width="4" height="5" viewBox="0 0 4 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect y="0.5" width="4" height="4" rx="2" fill="white" fillOpacity="0.6" />
                </svg>
                <p>{moment(data.attributes.createdAt).format('MMM DD, YYYY')}</p>
            </div>
            <h1>{data.attributes.title}</h1>
            <NextImage
                layout="responsive"
                width={data.attributes.img.data.attributes.width}
                height={data.attributes.img.data.attributes.height}
                objectFit="contain"
                src={imageUrl}
                alt="Img"
                className={classes.image}
            />
            
            <div className={classes.content}>
                <ReactMarkdown children={data.attributes.content} />
            </div>
        </div>
    )
}

export default PostDetail