import React, { useState } from 'react'
import classes from './home.module.scss'
import PostCard from '../postCard/postCard'
import FeaturedPost from '../featuredPost/featuredPost'
import thumbnail3 from '../../assets/thumbnail-3.png'
import thumbnail2 from '../../assets/thumbnail-2.png'
import { Paginate } from '../pagination/paginate'
import Pagination from '../pagination/pagination'

const mockPosts =
    [
        {
            "id": 1,
            "thumbnail": thumbnail2,
            "category": "Internet",
            "date": "June 28, 2021",
            "title": "How to design a product that can grow itself 10x in year",
            "shortDescription": "Auctor Porta. Augue vitae diam mauris faucibus blandit elit per, feugiat leo dui orci. Etiam vestibulum. Nostra netus per conubia dolor.",
        },
        {
            "id": 2,
            "thumbnail": thumbnail3,
            "category": "9 to 5",
            "date": "June 22, 2021",
            "title": "The More Important the Work, the More Important the Rest",
            "shortDescription": "Suitable Quality is determined by product users, clients or customers, not by society in general. For example, a low priced product may be viewed as having high.",
        },
        {
            "id": 3,
            "thumbnail": thumbnail2,
            "category": "Inspirations",
            "date": "June 18, 2021",
            "title": "Email Love - Email Inspiration, Templates and Discovery",
            "shortDescription": "Consider that for a moment: everything we see around us is assumed to have had a cause and is contingent upon something else.",
        },
        {
            "id": 4,
            "thumbnail": thumbnail3,
            "category": "Internet",
            "date": "June 28, 2021",
            "title": "How to design a product that can grow itself 10x in year",
            "shortDescription": "Auctor Porta. Augue vitae diam mauris faucibus blandit elit per, feugiat leo dui orci. Etiam vestibulum. Nostra netus per conubia dolor.",
        },
        {
            "id": 5,
            "thumbnail": thumbnail2,
            "category": "9 to 5",
            "date": "June 22, 2021",
            "title": "The More Important the Work, the More Important the Rest",
            "shortDescription": "Suitable Quality is determined by product users, clients or customers, not by society in general. For example, a low priced product may be viewed as having high.",
        },
        {
            "id": 6,
            "thumbnail": thumbnail3,
            "category": "Inspirations",
            "date": "June 18, 2021",
            "title": "Email Love - Email Inspiration, Templates and Discovery",
            "shortDescription": "Consider that for a moment: everything we see around us is assumed to have had a cause and is contingent upon something else.",
        }
    ];
const Home = ({ data }) => {
    const heroPost = data.data[0];
    const morePosts = data.data.slice(1)

    const [currentPage, setCurrentPage] = useState(1);
    const pageSize = 3;

    const onPageChange = (page) => {
        setCurrentPage(page);
    };

    const paginatedPosts = Paginate(morePosts, currentPage, pageSize);
    return (
        <div className={classes.rootHome}>
            <>
                {heroPost && (
                    <FeaturedPost post={heroPost.attributes}></FeaturedPost>
                )}
                <div className={classes.cardMain}>
                    {paginatedPosts.map((post, index) => (
                        <PostCard post={post.attributes} key={index} />
                    ))}

                </div>
                <Pagination
                    items={morePosts.length} // 100
                    currentPage={currentPage} // 1
                    pageSize={pageSize} // 10
                    onPageChange={onPageChange}
                />

            </>
        </div>
    )
}

export default Home