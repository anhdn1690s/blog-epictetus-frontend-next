import React from 'react'
import classes from './footer.module.scss'

const Footer = () => {
    return (
        <div className={classes.rootFooter}>
            Copyright (c) 2023 - Design By Anhdn18
        </div>
    )
}

export default Footer